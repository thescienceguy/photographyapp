// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDLseSEqyqFPLZYqGasTU7-X-7nTkitJq8",
    authDomain: "kris-turner-photography.firebaseapp.com",
    databaseURL: "https://kris-turner-photography-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "kris-turner-photography",
    storageBucket: "kris-turner-photography.appspot.com",
    messagingSenderId: "697955362054",
    appId: "1:697955362054:web:eb16d4443fe83ad0beaa87",
    measurementId: "G-CRBLK7RZ6L"
  }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
