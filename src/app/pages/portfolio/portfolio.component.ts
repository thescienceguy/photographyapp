import { GalleryItemServices } from './../../components/gallery-item/gallery-item.services';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  gallery: any;

  constructor(service: GalleryItemServices) { 
    this.gallery = service.getGalleryItems();
  }

  ngOnInit() {

  }

}
