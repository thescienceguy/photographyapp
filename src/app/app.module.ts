import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AppComponent } from './app.component';
import { LoaderComponent } from './components/loader/loader.component';
import { HeaderComponent } from './components/header/header.component';
import { BackgroundImageComponent } from './components/background-image/background-image.component';
import { FollowWrapperComponent } from './components/follow-wrapper/follow-wrapper.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HomeComponent } from './pages/home/home.component';
import { PortfolioComponent } from './pages/portfolio/portfolio.component';
import { GalleryItemComponent } from './components/gallery-item/gallery-item.component';
import { GalleryItemServices } from './components/gallery-item/gallery-item.services';
import { environment } from 'src/environments/environment';



@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent,
    HeaderComponent,
    BackgroundImageComponent,
    FollowWrapperComponent,
    SidebarComponent,
    HomeComponent,
    PortfolioComponent,
    GalleryItemComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      { path: "", component: HomeComponent },
      { path: "portfolio", component: PortfolioComponent}
    ])
  ],
  providers: [
    GalleryItemServices
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
