import { Component } from '@angular/core';
import { trigger, transition, state, style, animate } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger("fade", [
      transition('void => *', [
        style({ opacity: 0 }),
        animate("500ms 1000ms ease-in")
      ])
    ])
  ]
})
export class AppComponent {
  title = 'app';
}
