import { AngularFireDatabase } from '@angular/fire/database';
export class GalleryItemServices {

  galleryItems: any;

  constructor(db: AngularFireDatabase) {
    db.list("/gallery").valueChanges().subscribe(gallery => {
      this.galleryItems = gallery;
      console.log(gallery);
    });
  }

  getGalleryItems() {

    return this.galleryItems;

  }

}