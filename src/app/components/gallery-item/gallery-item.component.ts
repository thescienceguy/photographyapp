import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'gallery-item',
  templateUrl: './gallery-item.component.html',
  styleUrls: ['./gallery-item.component.css']
})
export class GalleryItemComponent implements OnInit {

  @Input() photo: any;
  @Input('url') imageUrl = "";

  constructor() { }

  ngOnInit() {
    this.imageUrl = "assets/" + this.photo.imageUrl;
  }

}
