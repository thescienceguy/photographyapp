import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css'],
  animations: [
    trigger("fadeOut", [
      transition('void => *', [
        style({ opacity: 1, display: "block"}),
        animate("1000ms 100ms ease-out")
      ])
    ])
  ]
})
export class LoaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
