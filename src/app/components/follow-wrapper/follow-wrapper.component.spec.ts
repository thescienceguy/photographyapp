import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowWrapperComponent } from './follow-wrapper.component';

describe('FollowWrapperComponent', () => {
  let component: FollowWrapperComponent;
  let fixture: ComponentFixture<FollowWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
