import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'background-image',
  templateUrl: './background-image.component.html',
  styleUrls: ['./background-image.component.css']
})
export class BackgroundImageComponent implements OnInit {

  @Input('has-overlay') hasOverlay = true;
  @Input('background-image') backgroundImage: string;

  constructor() { }

  ngOnInit() {
  }

}
